// JavaScript Document
					
					/*===============centriranje elementa u odnusu na Window ili Parent======================*/
(function($){
	//true = parent centar,false = window centar
	$.fn.centar = function(parent){
		if(parent){
			parent = this.parent();
		}else{
			parent =window;	
		}
		this.css({
			'position':'absolute',
			"top" : ((($(parent).height() - this.outerHeight()) / 2 ) + $(parent).scrollTop() +"px"),
			"left" : ((($(parent).width() - this.outerWidth()) / 2 ) + $(parent).scrollTop() +"px")
		});
		return this;
	}
})(jQuery);
					
					/*=====================PROVJERA JELI U VIEWPORT ILI NIJE===================================*/
(function($){
$.fn.display = function(){
			// window top
		var windowTop =$(window).scrollTop();
		var  windowHeight= $(window).height();
			//elem coordinat
		var elem = $(this).offset().top;
		//alert(elem);
		var	elemHeight = $(this).height();
		var elemBottom = elem + elemHeight;
			//trenutna pozicija elementa
		var currentPosition = elem - windowTop;
		//alert('Trenutna pozicija je' + currentPosition)
		
	if(windowTop < elemBottom && windowHeight  > currentPosition  ){
			//alert('vidi se');
			return true
	 }else{
		//alert('Ne vidi se');
	 	return false
	 }		
}//fn.display


})(jQuery);	



		
		/*============ dinamicko davanje dimenzija stranicama on page load(function) =======================*/
	function elementDimension(){
		var pages = $('.pages');
			//alert(demensionsWidth) 
		var dimensionsHeight = $(window).height() ;
		var newDimensionsHeight = dimensionsHeight;
			//alert(newDimensions)
		pages.css({
			'height':newDimensionsHeight + 250 
		});
	}
		
		/*============================ moving background effect (function) ======================*/
	
	function bcg_move(){
		$('div[data-type="background"]').each(function(){
		 var $bgobj = $(this);
		  var yPos = -($(window).scrollTop() / $bgobj.data('speed')); 
            var coords = '50% '+ yPos + 'px';
			            
            $bgobj.css({ backgroundPosition: coords });
		});
	 }
	
	
		/*============================= DISPLAY LOGO (box index) (function) =======================*/
	
	var i =0;
	function display_Logo(){
		var elem = $('.box');
			if(i < elem.length){
				$(elem[i]).removeClass('pull_Up');
				i++;
			}
			setTimeout(display_Logo,200);
	}
		
		/*============================= HOVER NA INDEX LOGO (function) =======================*/
	
	function logoIn(){
		$(this).css('border','#FFF 2px solid').addClass('scale_1-4');
		$(this).find('span').removeClass('scale_0').addClass('scale_1-4');
		$(this).find('p').addClass('scale_0');
		$(this).find('.descriptionIndex').css('display','block').addClass('scale_1-4');
	
	}
	function logoOut(){
		$(this).css('border','none').removeClass('scale_1-4');
		$(this).find('span').removeClass('scale_1-4').addClass('scale_0');
		$(this).find('p').removeClass('scale_0');
		$(this).find('.descriptionIndex').removeClass('scale_1-4').addClass('scale_0');

	}
	
	 /*============================= DISPLAY PORTOFOLIO BROWSERS EXAMPLES (function) =======================*/
	
//	function displayPortofolioBrowser(){
//		var portofolio = $('.browserPortofolio').display();
//		if(portofolio){
//			$('.browserPortofolio').delay(5000).removeClass('scale_0').addClass('scale_1');
//
//		}
//	}
	

	
	/*============================dinamicko mjenjanje bcg portofolio page(slider)(function)========*/
	
	function slider(target){
			$('#description').removeClass('moveRight');
			$('#slider').fadeOut(2000,function(){
				$(this).remove();//remove icon
			});
			$('#portofolio').prepend($('<div id="slider" data-type="background" data-speed="10" ><div id="overlay">	a	 				</div><div id="description"></div></div>').addClass(target).fadeIn(1000));
			
			setTimeout(function(){
				$('#description').addClass('moveRight');
				$('#description').load('fragmants.html #'+ target+'');
			},1000);
	}

	/*======================== provjera slider_offset.top pozicije (function) ===================*/
	
	function provjera_SliderTop(){
		var elem =$('#portofolio').offset().top;
		var windowTop = $(window).scrollTop()
		var currentPosition = elem - windowTop;
		 if(currentPosition!=0){
				$('html,body').animate({
					scrollTop:elem
				
				},500,'swing');
			}
	}
	/*====================== #SERVICES TITLE DETEKCIJA IS_ON_SCREEN I DAVANJE CLASS-E (function) ===================*/

	function displayTitle(){
		$('.scaleTitle').each(function() {
            var curent= $(this).display();
			if(curent){
				$(this).addClass('scaleTitle-in');
			}else{
				$(this).removeClass('scaleTitle-in');
			}
        });
	}
	
	
	
	
	/*====================== #SERVICES CONTENT DETEKCIJA IS_ON_SCREEN I DAVANJE CLASS-E(function) ===================*/
	
	
	function displayServices(){
		$('.scaleServices').each(function() {
            var curent= $(this).display();
			if(curent){
				$(this).addClass('scaleServices-in');
			}else{
				$(this).removeClass('scaleServices-in');
			}
        });
		
	}
	
		/*================================ MODAL WINDOW(function) =================================================*/
	
	function modal_window(url){	
		event.preventDefault();
		//var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$(url).modal('open');
		} else {
			$.get(url, function(data) {
				var title=$(data).filter('title').text();
				$('<div class="modal  fade" id="#myModal"><div class="modal-dialog"><div class="modal-content"><div 			class="modal-header"><h4 class="modal-title" id="myModalLabel">' + title + '</h4></div><div class="modal-body">' + data + '</div></div></div></div>').modal();
			})
		}
	 };
	 
	


	 /*================================ PROGRESS BAR(function) =================================================*/
	 
	//function progres(view){
	//	$('.progres').each(function() {
	//		var index = $(this);
	//		var current = $(this).data('progres');
	//		var view = $('.skillsWraper').attr('data-view');
	//		if  ( view == 'false' ){
	//		var width = 0;
	//		var interval = setInterval(function(){
	//			width = width + 1;
	//			index.css('width', width + '%')
	//			if(width == current ){
	//				clearInterval(interval);
	//			}
	//		},10)}
	//	});
	//}

	/*=============================== DETEKCIJA I  DISPLAY PROGRESS BAR-a(function)==============================*/
	
	//function displayProgres(){
	//
	//	$('.progresBar').each(function() {
     //       var curent= $(this).display();
	//		var curent = $('.skillsWraper').display();
	//		if(curent){
	//			progres();
	//			$('.progresTitle').find('span').css('opacity','1');
	//			 $('.skillsWraper').attr('data-view','true');
	//		}else{
	//			 $('.skillsWraper').attr('data-view','false');
	//			 $('.progresTitle').find('span').css('opacity','0');
	//		}
     //   });
	//}


/*================================ DISPLAY SKILLS(function) =================================================*/

    function skillsDisplay(){
        $('.skillsWraper').each(function(){
           var current = $(this).display();
            var elem = $(this).children().find('.col-lg-3');
            if(current) {
                $(this).removeClass('scale_0').addClass('scale_1')
            }else{
                $(this).removeClass('scale_1').addClass('scale_0')
            }
        });
    };


$(document).ready(function(e) {
	
	elementDimension();//dinamicko davanje dimenzija stranicama	
	display_Logo()//display loga on page load
	
				  /*=======================on scroll event kontrola====================================*/
	$(window).scroll(function(){
		bcg_move()//effect za moving background
		onScrollMenu();// davanje active class menu
		menuFixed();
		//displayPortofolioBrowser()//detecting and display portofolio browsers
		displayServices()//detecting and display ABOUT CONTENT
		displayTitle()//detecting and display TITLE na stranici
		//displayProgres()//detecting and display PROGRES BAR na about us
        skillsDisplay()

		
		
		 var elem = $('.wraperIndex_menu')//.display();
		 if(elem.display()=== true){
			var i = 0
			function display_Logo(){
				var elem = $('.box');
					if(i < elem.length){
						$(elem[i]).removeClass('pull_Up');
						i++;
						setTimeout(display_Logo,200);
					}
			}
			display_Logo();
		 }else{
			$('.box').addClass('pull_Up');
		 }


		
	})//scroll

	
				 /*================regulisanje dinamickog podesavanja na 'resize'======================*/
	
	$(window).on('resize',function(){
		elementDimension();//dinamicko davanje dimenzija 		
	});
				/*================= kontrola menu-a ,i putanja parallax scroll effecta =================*/
	
	$('.menu').find('a').on('click',function(e){
		e.preventDefault();
		var target = this.hash
		var $target = $(target);
		$('html,body').stop().animate({
			scrollTop : $target.offset().top		
		},1500,function(){
				window.location = target;
		})	
	
	});	//click
	
					/*====================== HOVER NA ELEMENTE =====================================*/							
	
	$('.box').hover(logoIn,logoOut)//hover na logo index									
	
			
				
				/*====================== potrofolio  mjenjanje bcg image(slider) =============================*/

	$('.bg-thumbs').on('click',function(){
		event.preventDefault();
		$('.currentBcg').removeClass('currentBcg');
		$(this).addClass('currentBcg');
		 var target = $(this).find('a').attr('href');
		 
		provjera_SliderTop()//provjera this top-a
		slider(target);//slider for background 
	});
	
				/*====================== POKRETANJE MODAL WINDOW (BOOTSTRAP) =============================*/
	$('[data-toggle="modal"]').click(function(e) {
		var url = $(this).attr('href');
		modal_window(url)//POKRETANJE MODAL WINDOW
	
	});
	







				
				
				
				
				
				 /*====================FUNKCIJE KOJE MORAJU BITI U READY-U==================================*/
		
		
		/*==============kordinate elemenata, i reagiranje -a na click i scroll(function)=============*/	
	
	var topMenu = $(".menu");
    	//menu kordinati
	var topMenuHeight = topMenu.outerHeight()+15;
    var menuItems = topMenu.find("a");
		scrollItems = menuItems.map(function(){
		  var item = $($(this).attr("href"));
		  if (item.length) { return item; }
		});
	
		
		/*===============dinamicko izracunavanje trenutne pozicije on scrollMenu (function)====================*/
	
	function onScrollMenu(){
		//kordinati od topa
	   var fromTop = $(this).scrollTop()+topMenuHeight;
		// uzimanje id od trenutnog skrolanog
	   var cur = scrollItems.map(function(){
		 if ($(this).offset().top < fromTop)
		   return this;
	   });
	   // uzimanje id od trenutnog elementa
	   cur = cur[cur.length-1];
	   var id = cur && cur.length ? cur[0].id : "";
	   //dodavanje i brisanje klasa active u menu
	   menuItems
	   .parent().removeClass("active")
	   .end().filter("[href=#"+id+"]").parent().addClass("active");
	};//onScrool
	
	
	/*================================= FIXED NAV-BAR(FUNCTION) ===============================================*/
	
	var menu = $('.menu').offset().top;
	function menuFixed(){
		var windowTop = $(window).scrollTop();
			if(windowTop >= menu){
					$('.menu').addClass('navbar-fixed-top menuFixed ');
					$('.navbar-nav').addClass('pull-right');
					$('.navbar-brand').css('display','block');
			}else{
					$('.menu').removeClass('navbar-fixed-top menuFixed pull-right');
					$('.nav').removeClass('pull-right');
					$('.navbar-brand').css('display','none');	
				}
	}





	


});//ready


		
		
		
								
								
								
								
								/*===============EASING (PLUGIN)====================*/
			
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});
		
			
			
			